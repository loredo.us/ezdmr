from enum import Enum
from typing import List, Union

class DMRZone(object):
    
    def __init__(
                    self,
                    name: str
                ):
        self._name = name
        self._ch_bank_a = []
        self._ch_bank_b = []

class DMRContactCallType(Enum):
    GROUP_CALL = 1
    PRIVATE_CALL = 2
    ALL_CALL = 3

class DMRContact(object):
    # Validation parameters
    NAME_MAX_LENGTH = 16
    CALL_ID_MAX = 99999999
    CALL_ID_MIN = 0

    def __init__(
                    self,
                    name: str,
                    call_id: int,
                    call_type: DMRContactCallType = DMRContactCallType.GROUP_CALL
                ):
        self.name = name
        self.call_id = call_id
        self.call_type = call_type

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, value: str):
        value = value.strip()
        try:
            assert isinstance(value, str), "Channel name must be a string value"
            assert len(value) > 0, "Channel name must be at least 1 character long"
            assert len(value) <= self.NAME_MAX_LENGTH, f"Channel name can't be longer than {self.NAME_MAX_LENGTH} characters"
        except AssertionError as raised_exception:
            raise AttributeError(f'Invalid value for channel name: {raised_exception.args[0]}')
        self._name = value

    @property
    def call_id(self) -> int:
        return self._call_id

    @call_id.setter
    def call_id(self, value: int):
        try:
            assert isinstance(value, int), "Call ID must be an integer value"
            assert value > self.CALL_ID_MIN, f"Call ID must be greater than {self.CALL_ID_MIN}"
            assert value <= self.CALL_ID_MAX, f"Call ID must be less than {self.CALL_ID_MAX}"
        except AssertionError as raised_exception:
            raise AttributeError(f'Invalid value for Call ID: {raised_exception.args[0]}')
        self._call_id = value

    @property
    def call_type(self) -> DMRContactCallType:
        return self._call_type

    @call_type.setter
    def call_type(self, value: DMRContactCallType):
        try:
            assert isinstance(value, DMRContactCallType), "Call Type must be of type DMRContactCallType"
        except AssertionError as raised_exception:
            raise AttributeError(f'Invalid value for Call Type: {raised_exception.args[0]}')
        self._call_type = value

class DMRGroupList(object):
    # Validation Parameters
    MAX_LIST_SIZE = 32
    NAME_MAX_LENGTH = 16

    def __init__(self, name: str):
        self.name = name
        self.members = []
    
    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, value: str):
        value = value.strip()
        try:
            assert isinstance(value, str), "Group List name must be a string value"
            assert len(value) > 0, "Group List name must be at least 1 character long"
            assert len(value) <= self.NAME_MAX_LENGTH, f"Group List name can't be longer than {self.NAME_MAX_LENGTH} characters"
        except AssertionError as raised_exception:
            raise AttributeError(f'Invalid value for Group List name: {raised_exception.args[0]}')
        self._name = value

    @property
    def members(self) -> List[DMRContact]:
        return self._members

    @members.setter
    def members(self, value: List[DMRContact]):
        try:
            assert isinstance(value, list), "Group List members must be a list"
            assert all([isinstance(member, DMRContact) for member in value]), "All Group List members must be DMRContact objects"
            assert len(value) <= self.MAX_LIST_SIZE, f"Group Lists cannot be longer than {self.MAX_LIST_SIZE} members"
        except AssertionError as raised_exception:
            raise AttributeError(f'Invalid value for Group List members: {raised_exception.args[0]}')
        self._members = value
    
    def append(self, value: Union[List[DMRContact], DMRContact]):
        if isinstance(value, list):
            for item in value:
                self.append(item)
        else:
            try:
                assert isinstance(value, DMRContact), "Group List members must be DMRContact objects"
                assert len(self._members) < self.MAX_LIST_SIZE, f"Group Lists cannot be longer than {self.MAX_LIST_SIZE} members"
            except AssertionError as raised_exception:
                raise AttributeError(f'Could not append to Group List members: {raised_exception.args[0]}')
            self._members.append(value)